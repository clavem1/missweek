(function(){
    module.exports = function(app){
        const controller = require("./post.controller");
        app.route("/post")
            .get(controller.list)

        app.route("/post/:id")
            .delete(controller.delete)
            .get(controller.get)

        app.route("/post/:user")
            .get(controller.listByUser)
    }
})();