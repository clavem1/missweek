(function(){
    const Post = require("./post.modele").PostModel
    const User = require("./../users/user.modele").UserModel
    const async = require('async')
    module.exports = {
        list: (req, res)=>{
            Post.find({}).populate('user')
            .populate({
                path:     'comments',
                populate: { path:  'user', model: 'User' }
            }).populate({
                path:     'likes',
                populate: { path:  'user', model: 'User' }
            }).sort({date_publication: 'desc'})
                .exec((error, rep)=>{
                if(error){
                    console.log("Erreur lors de la récupération des posts ", error)
                }
                //console.log(rep);
                res.json({
                    body:rep,
                    status:'success'
                })
            });
        },
        listByUser: (req, res)=>{
            Post.find({user:req.params.user}).populate({
                path:     'comments',
                populate: { path:  'user', model: 'User' }
            }).populate({
                path:     'likes',
                populate: { path:  'user', model: 'User' }
            }).sort({date_publication: 'desc'})
                .exec((error, rep)=>{
                if(error){
                    console.log("Erreur lors de la récupération des posts ", error)
                }
                console.log(rep);
                res.json({
                    body:rep,
                    status:'success'
                })
            });
        },
        /*add: (req, res)=>{
            req.body.date_publication = new Date()
            let post = new Post(req.body)
            post.save((error, rep)=>{
                if(error){
                    console.log("Echec ajout ", error)
                    res.status(500).json({
                        status : "echec",
                        body : "L'enregistrement du post a échoué"
                    })
                }else{
                    res.json({
                        status : "success",
                        body : rep
                    })
                }
            })
        },*/
        delete: (req, res)=>{
            Post.deleteOne({_id:req.params.id}, (error, rep)=>{
                if(error){
                    console.log("Erreur suppression", error)
                    res.status(500).json({
                        status : "echec",
                        body : "La suppression du post a échoué"
                    })
                }else{
                    res.json({
                        body:rep,
                        status:'success'
                    })
                }
            })
        },
        get: (req, res)=>{
            Post.findOne({_id:req.params.id}, (error, rep)=>{
                if(error){
                    console.log("Erreur", error)
                    res.status(404).json({
                        status : "echec",
                        body : "Aucun post trouvé"
                    })
                }else{
                    res.json({
                        body:rep,
                        status:'success'
                    })
                }
            })
        }
    }
})();