(function(){
    const mongoose = require("mongoose")
    const Schema = mongoose.Schema

    const postSchema = new Schema({
        date_publication: {type:Date, required:true},
        user: {type: Schema.ObjectId, ref: 'User', required:true},
        media_url: {type:String, required:true},
        comments: [{ type: Schema.Types.ObjectId, ref: 'Comment' }],
        likes: [{ type: Schema.Types.ObjectId, ref: 'Like' }]
    })
    module.exports={
        PostModel: mongoose.model("Post", postSchema)
    }
})()