//fonction qui s'appelle elle-même
(function(){
    module.exports = function(app){
        const controller = require("./user.controller");
        app.route("/user")
            .get(controller.list)
            .post(controller.add);

        app.route("/user/:id")
            .delete(controller.delete)
            .put(controller.update)
            .get(controller.get);

        app.route("/login")
            .post(controller.login)
    }
})();