(function() {
  const mongoose = require("mongoose");
  const Schema = mongoose.Schema;

  const userSchema = new Schema({
    username: { type: String, required: true },
    telephone: { type: String, required: true },
    password: { type: String, required: true },
    active: { type: Boolean, required: true, default: true },
    role: { type: String, default: "user" }
  });
  module.exports = {
    UserModel: mongoose.model("User", userSchema)
  };
})();
