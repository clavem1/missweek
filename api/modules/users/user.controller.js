(function(){
    const User = require("./user.modele").UserModel
    module.exports = {
        list : (req, res)=>{
            //{} quand il n'y a pas de conditions au select
            User.find({}, (error, rep)=>{
                if(error){
                    console.log("Erreur lors de la récupération des utilisateurs ", error)
                }
                res.json({
                    body:rep,
                    status:'success'
                })
            })
        },
        add : (req, res)=>{
            let user = new User(req.body)
            user.save((error, rep)=>{
                if(error){
                    console.log("Echec ajout ", error)
                    res.status(500).json({
                        status : "echec",
                        body : "L'enregistrement de l'utilisateur a échoué"
                    })
                }else{
                    res.json({
                        status : "success",
                        body : rep
                    })
                }
            })
        },
        delete: (req, res)=>{
            User.deleteOne({_id:req.params.id}, (error, rep)=>{
                if(error){
                    console.log("Erreur suppression ", error)
                    res.status(500).json({
                        status : "echec",
                        body : "La suppression de l'utilisateur a échoué"
                    })
                }else{
                    res.json({
                        body:rep,
                        status:'success'
                    })
                }
            })
        },
        update: (req, res)=>{
            User.findOneAndUpdate({_id:req.params.id}, req.body, {new: true}, (error, rep)=>{
                if(error){
                    console.log("Erreur update utilisateur ", error)
                    res.status(500).json({
                        status : "echec",
                        body : "La modification de l'utilisateur a échoué"
                    })
                }else{
                    res.json({
                        body:rep,
                        status:'success'
                    })
                }
            })
        },
        get: (req, res)=>{
            User.findOne({_id:req.params.id}, (error, rep)=>{
                if(error){
                    console.log("Erreur ", error)
                    res.status(404).json({
                        status : "echec",
                        body : "Utilisateur non trouvé"
                    })
                }else{
                    res.json({
                        body:rep,
                        status:'success'
                    })
                }
            })
        },
        login : (req, res)=>{
            User.findOne({pseudo:req.body.pseudo, password:req.body.password}, (error, rep)=>{
                if(error){
                    console.log("Erreur ", error)
                    res.status(401).json({
                        status : "echec",
                        body : "L'utilisateur n'a pas été trouvé"
                    })
                }else{
                    res.json({
                        body:rep,
                        status:'success'
                    })
                }
            })
        }
    }
})();