(function(){
    const mongoose = require("mongoose")
    const Schema = mongoose.Schema

    const likeSchema = new Schema({
        user: {type: Schema.ObjectId, ref: 'User', required:true},
        post: {type: Schema.ObjectId, ref: 'Post', required:false},
        delire: {type: Schema.ObjectId, ref: 'Delire', required:false},
        date:{type:Date, required:true}
    })
    module.exports={
        LikeModel: mongoose.model("Like", likeSchema)
    }
})()