(function(){
    module.exports = function(app){
        const controller = require("./like.controller");
        app.route("/like")
            .post(controller.add)

        app.route("/like/:post")
            .get(controller.list)
            
        app.route("/like/:id")
            .delete(controller.delete)
    }
})();