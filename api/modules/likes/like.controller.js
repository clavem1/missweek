(function(){
    const Like = require("./like.modele").LikeModel
    const Post = require("./../posts/post.modele").PostModel
    const Delire = require('./../delires/delire.modele').DelireModel
    module.exports = {
        list : (req, res)=>{
            Like.find({post: req.params.post}, (error, rep)=>{
                if(error){
                    console.log("Erreur lors de la récupération des likes ", error)
                }
                res.json({
                    body:rep,
                    status:'success'
                })
            })
        },
        add : (req, res)=>{
            req.body.date = new Date()
            let like = new Like(req.body)
            like.save((error, rep)=>{
                if(error){
                    console.log("Echec ajout ", error)
                    res.status(500).json({
                        status : "echec",
                        body : "L'enregistrement du like a échoué"
                    })
                }else{
                    res.json({
                        status : "success",
                        body : rep
                    })
                }
            })
            if(req.body.post) {
                Post.findOne({_id: req.body.post}, function (error, rep) {
                    rep.likes.push(like);
                    rep.save()
                })
            }else{
                Delire.findOne({_id: req.body.delire}, function (error, rep) {
                    rep.likes.push(like);
                    rep.save()
                })
            }
        },
        delete: (req, res)=>{
            Like.deleteOne({_id:req.params.id}, (error, rep)=>{
                if(error){
                    console.log("Erreur suppression ", error)
                    res.status(500).json({
                        status : "echec",
                        body : "La suppression du like a échoué"
                    })
                }else{
                    res.json({
                        body:rep,
                        status:'success'
                    })
                }
            })
        }
    }
})();