(function(){
    module.exports = function(app){
        const controller = require("./messagerie.controller");
        app.route("/messagerie")
            .post(controller.add)

        app.route("/messagerie/:user1/:user2")
            .get(controller.list)

        app.route("/messagerie/:id")
            .delete(controller.delete)
            .get(controller.get)
    }
})();