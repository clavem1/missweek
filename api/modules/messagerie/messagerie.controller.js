(function(){
    const Messagerie = require("./messagerie.modele").messagerieModel
    module.exports = {
        list: (req, res)=>{
            Messagerie.find({ 
                $or : [
                    { $and : [ { sender : req.params.user1 }, { receiver : req.params.user2 } ] },
                    { $and : [ { sender : req.params.user2 }, { receiver : req.params.user1 }  ] }
                ]
             })
            .populate('sender')
            .populate('receiver')
                .exec((error, rep)=>{
                if(error){
                    console.log("Erreur lors de la récupération des messages ", error)
                }
                //console.log(rep);
                res.json({
                    body:rep,
                    status:'success'
                })
            });
        },
        add: (req, res)=>{
            req.body.date = new Date()
            let messagerie = new Messagerie(req.body)
            messagerie.save((error, rep)=>{
                if(error){
                    console.log("Echec ajout ", error)
                    res.status(500).json({
                        status : "echec",
                        body : "L'envoi du message a échoué"
                    })
                }else{
                    res.json({
                        status : "success",
                        body : rep
                    })
                }
            })
        },
        delete: (req, res)=>{
            Messagerie.deleteOne({_id:req.params.id}, (error, rep)=>{
                if(error){
                    console.log("Erreur suppression", error)
                    res.status(500).json({
                        status : "echec",
                        body : "La suppression du message a échoué"
                    })
                }else{
                    res.json({
                        body:rep,
                        status:'success'
                    })
                }
            })
        },
        get: (req, res)=>{
            Messagerie.findOne({_id:req.params.id}, (error, rep)=>{
                if(error){
                    console.log("Erreur", error)
                    res.status(404).json({
                        status : "echec",
                        body : "Aucun post trouvé"
                    })
                }else{
                    res.json({
                        body:rep,
                        status:'success'
                    })
                }
            })
        }
    }
})();