(function(){
    const mongoose = require("mongoose")
    const Schema = mongoose.Schema

    const messagerieSchema = new Schema({
        date: {type:Date, required:true},
        isRead: {type:Boolean, required:false},
        sender: {type: Schema.ObjectId, ref: 'User', required:true},
        receiver: {type: Schema.ObjectId, ref: 'User', required:true},
        message_body: {type: String, required:false},
        media_url: {type:String, required:false}
    })
    module.exports={
        messagerieModel: mongoose.model("Messagerie", messagerieSchema)
    }
})()