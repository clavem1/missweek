(function(){
    const mongoose = require("mongoose")
    const Schema = mongoose.Schema

    const commentSchema = new Schema({
        user: {type: Schema.ObjectId, ref: 'User', required:true},
        post: {type: Schema.ObjectId, ref: 'Post', required:false},
        delire: {type: Schema.ObjectId, ref: 'Delire', required:false},
        comment_body: {type:String, required:true},
        date:{type:Date, required:true}
    })
    module.exports={
        CommentModel: mongoose.model("Comment", commentSchema)
    }
})()