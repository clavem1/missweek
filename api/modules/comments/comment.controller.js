(function(){
    const Comment = require("./comment.modele").CommentModel
    const Post = require("./../posts/post.modele").PostModel
    const Delire = require('./../delires/delire.modele').DelireModel
    module.exports = {
        list : (req, res)=>{
            Comment.find({post: req.params.post}, (error, rep)=>{
                if(error){
                    console.log("Erreur lors de la récupération des commentaires ", error)
                }
                res.json({
                    body:rep,
                    status:'success'
                })
            }).populate('user').sort({'data':'desc'})
        },
        add : (req, res)=>{
            req.body.date = new Date()
            let comment = new Comment(req.body)
            comment.save((error, rep)=>{
                if(error){
                    console.log("Echec ajout ", error)
                    res.status(500).json({
                        status : "echec",
                        body : "L'enregistrement du commentaire a échoué"
                    })
                }else{
                    res.json({
                        status : "success",
                        body : rep
                    })
                }
            })
            if(req.body.post) {
                Post.findOne({_id: req.body.post}, function (error, rep) {
                    rep.comments.push(comment);
                    rep.save()
                })
            }else{
                Delire.findOne({_id: req.body.delire}, function (error, rep) {
                    rep.comments.push(comment);
                    rep.save()
                })
            }
        },
        delete: (req, res)=>{
            Comment.deleteOne({_id:req.params.id}, (error, rep)=>{
                if(error){
                    console.log("Erreur suppression ", error)
                    res.status(500).json({
                        status : "echec",
                        body : "La suppression du commentaire a échoué"
                    })
                }else{
                    res.json({
                        body:rep,
                        status:'success'
                    })
                }
            })
        },
        update: (req, res)=>{
            Comment.findOneAndUpdate({_id:req.params.id}, req.body, {new: true}, (error, rep)=>{
                if(error){
                    console.log("Erreur update commentaire ", error)
                    res.status(500).json({
                        status : "echec",
                        body : "La modification du commentaire a échoué"
                    })
                }else{
                    res.json({
                        body:rep,
                        status:'success'
                    })
                }
            })
        },
        get: (req, res)=>{
            Comment.findOne({_id:req.params.id}, (error, rep)=>{
                if(error){
                    console.log("Erreur ", error)
                    res.status(404).json({
                        status : "echec",
                        body : "Commentaire non trouvé"
                    })
                }else{
                    res.json({
                        body:rep,
                        status:'success'
                    })
                }
            })
        }
    }
})();