(function(){
    module.exports = function(app){
        const controller = require("./comment.controller");
        app.route("/comment")
            .post(controller.add)

        app.route("/comment/:post")
            .get(controller.list)
            
        app.route("/comment/:id")
            .delete(controller.delete)
            .put(controller.update)
            .get(controller.get)
    }
})();