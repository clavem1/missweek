(function(){
    module.exports = function(app){
        const controller = require("./delire.controller");
        app.route("/delire")
            .get(controller.list)

        app.route("/delire/:id")
            .delete(controller.delete)
            .get(controller.get)

        app.route("/delire/:user")
        .get(controller.listByUser)
    }
})();