(function(){
    const mongoose = require("mongoose")
    const Schema = mongoose.Schema

    const delireSchema = new Schema({
        date_publication: {type:Date, required:true},
        user: {type: Schema.ObjectId, ref: 'User', required:true},
        media_url: {type:String, required:false},
        text: {type:String, required:false},
        comments: [{ type: Schema.Types.ObjectId, ref: 'Comment' }],
        likes: [{ type: Schema.Types.ObjectId, ref: 'Like' }]
    })
    module.exports={
        DelireModel: mongoose.model("Delire", delireSchema)
    }
})()