(function(){
    const Delire = require("./delire.modele").DelireModel
    module.exports = {
        list: (req, res)=>{
            Delire.find({}).populate('user')
            .populate({
                path:     'comments',
                populate: { path:  'user', model: 'User' }
            }).populate({
                path:     'likes',
                populate: { path:  'user', model: 'User' }
            }).sort({date_publication: 'desc'})
                .exec((error, rep)=>{
                if(error){
                    console.log("Erreur lors de la récupération des délires ", error)
                }
                //console.log(rep);
                res.json({
                    body:rep,
                    status:'success'
                })
            });
        },
        listByUser: (req, res)=>{
            Delire.find({user:req.params.user}).populate({
                path:     'comments',
                populate: { path:  'user', model: 'User' }
            }).populate({
                path:     'likes',
                populate: { path:  'user', model: 'User' }
            }).sort({date_publication: 'desc'})
                .exec((error, rep)=>{
                if(error){
                    console.log("Erreur lors de la récupération des délires ", error)
                }
                console.log(rep);
                res.json({
                    body:rep,
                    status:'success'
                })
            });
        },
        delete: (req, res)=>{
            Delire.deleteOne({_id:req.params.id}, (error, rep)=>{
                if(error){
                    console.log("Erreur suppression", error)
                    res.status(500).json({
                        status : "echec",
                        body : "La suppression du délire a échoué"
                    })
                }else{
                    res.json({
                        body:rep,
                        status:'success'
                    })
                }
            })
        },
        get: (req, res)=>{
            Delire.findOne({_id:req.params.id}, (error, rep)=>{
                if(error){  
                    console.log("Erreur", error)
                    res.status(404).json({
                        status : "echec",
                        body : "Aucun délire trouvé"
                    })
                }else{
                    res.json({
                        body:rep,
                        status:'success'
                    })
                }
            })
        }
    }
})();