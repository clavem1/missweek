const express = require("express");
const mongoose = require("mongoose");
const bodyparser = require("body-parser");
const path = require("path"),
  acl = require("express-acl"),
  jwt = require("jsonwebtoken");
//permet de manipuler les fichiers dans le système(créer, récupérer, etc.)
const fs = require("fs");
//permet d'effectuer l'upload du fichier
const multer = require("multer");

const app = express();
require("dotenv").config();

//pour formater des données qui proviennent d'un formulaire (methode POST, PUT)
app.use(
  bodyparser.urlencoded({
    extended: true
  })
);
app.use(bodyparser.json());

/**************** CONFIG ACL *****************/
let configObject = {
  filename: "acl.json",
  path: "config"
};

acl.config(configObject);

//definir l'objet dans le quel se trouve le role dans la requete
acl.config({
  decodedObjectName: "user"
});

//cherchera role dans req.user.role
acl.config({
  roleSearchPath: "user.role"
});
app.use("/public", express.static(path.join(__dirname, "public")));
mongoose.connection.openUri(process.env.MONGO_URL, {}, error => {
  if (error) {
    console.log("Erreur lors de la connexion à la base de données ", error);
    process.exit();
  }
  init();
});

function init() {
  app.use(function(req, res, next) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader(
      "Access-Control-Allow-Methods",
      "GET, POST, OPTIONS, PUT, PATCH, DELETE"
    );
    res.setHeader(
      "Access-Control-Allow-Headers",
      "token, Content-Type, X-Requested-With"
    );
    res.setHeader("Access-Control-Allow-Credentials", true);
    if (req.method == "OPTIONS") return res.sendStatus(200);
    next();
  });

  require("./api/modules/auth/auth.routes")(app);
  /******
   * VERIFIER LA CONNECTIVITE DU USER
   */
  app.use((req, res, next) => {
    let token = req.headers.token;
    jwt.verify(token, "secret", function(err, decoded) {
      if (err) {
        return res.status(401).json({
          status: "error",
          body: "Vous n'êtes pas connecté"
        });
      } else {
        req.user = decoded.data;
        next();
      }
    });
  });

  /********* VERIFIER SI LE USER EST AUTORISE A
   * ACCEDER A LA ROUTE DEMANDEE
   */
  app.use(acl.authorize);
  app.post(
    "/audio",
    multer({ dest: "public/uploads/audios/" }).single("media"),
    (req, res) => {
      console.log(req.file);
      res.status(200).json({ n: "yessss" });
    }
  );
  app.post(
    "/post",
    multer({ dest: "public/uploads/posts/" }).single("media"),
    (req, res, next) => {
      const Post = require("./api/modules/posts/post.modele").PostModel;
      console.log(req.file);
      on = req.file.originalname.split(".");
      extension = on[on.length - 1];
      if (on.length < 2) {
        on = req.file.mimetype.split("/");
        extension = on[on.length - 1];
      }
      fs.rename(req.file.path, req.file.path + "." + extension, error => {
        if (error) {
          console.log(
            "Erreur lors de l'ajout de l'extension du fichier",
            error
          );
        } else {
          req.body.date_publication = new Date();
          req.body.media_url = req.file.filename + "." + extension;
          let post = new Post(req.body);
          post.save((error, rep) => {
            if (error) {
              console.log("Echec ajout ", error);
              res.status(500).json({
                status: "echec",
                body: "L'enregistrement du post a échoué"
              });
            } else {
              res.json({
                status: "success",
                body: rep
              });
            }
          });
        }
      });
    }
  );

  app.post(
    "/delire",
    multer({ dest: "public/uploads/delires/" }).single("media"),
    (req, res, next) => {
      const Delire = require("./api/modules/delires/delire.modele").DelireModel;
      req.body.date_publication = new Date();
      let delire = new Delire(req.body);
      if (req.file != null) {
        console.log(req.file);
        on = req.file.originalname.split(".");
        extension = on[on.length - 1];
        if (on.length < 2) {
          on = req.file.mimetype.split("/");
          extension = on[on.length - 1];
        }
        fs.rename(req.file.path, req.file.path + "." + extension, error => {
          if (error) {
            console.log(
              "Erreur lors de l'ajout de l'extension du fichier",
              error
            );
          } else {
            //req.body.date_publication = new Date()
            req.body.media_url = req.file.filename + "." + extension;
            let delire = new Delire(req.body);
            delire.save((error, rep) => {
              if (error) {
                console.log("Echec ajout ", error);
                res.status(500).json({
                  status: "echec",
                  body: "L'enregistrement du délire a échoué"
                });
              } else {
                res.json({
                  status: "success",
                  body: rep
                });
              }
            });
          }
        });
      } else {
        //req.body.date_publication = new Date()
        delire.save((error, rep) => {
          if (error) {
            console.log("Echec ajout ", error);
            res.status(500).json({
              status: "echec",
              body: "L'enregistrement du délire a échoué"
            });
          } else {
            res.json({
              status: "success",
              body: rep
            });
          }
        });
      }
    }
  );

  app.get("/", (req, res, next) => {
    res.send("Bienvenue sur Miss Weeks !");
    next();
  });

  require("./api/modules/users/user.route")(app);
  require("./api/modules/posts/post.route")(app);
  require("./api/modules/delires/delire.route")(app);
  require("./api/modules/comments/comment.route")(app);
  require("./api/modules/likes/like.route")(app);
  require("./api/modules/messagerie/messagerie.route")(app);

  app.listen(process.env.NODE_PORT, () => {
    console.log("Server listening on port ", process.env.NODE_PORT);
  });
}
